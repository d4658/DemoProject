package com.cgcs.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo")
public class DemoController {

	// This is a dummy endpoint
	@GetMapping("/dummy")
	public String dummyEndpoint() {
		return "This is a Dummy Endpoint created by Navya for CGCS";
	}

	// Added Actuator as dependency so hit /actuator/health for health endpoint

}
